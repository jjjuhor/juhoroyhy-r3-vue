import scrollTriggers from 'scroll-triggers';
import 'lazysizes';


export default {
	init() {
		// JavaScript to be fired on all pages


	},
	finalize() {
		// JavaScript to be fired on all pages, after page specific JS is fired
		scrollTriggers([{
			el: '.event',
			className: 'visible',
			offset: 200,
		}]);


		jQuery('.gallery').masonry();
		jQuery('#front .bg').parallaxjs();


		jQuery(document).ready(function($) {
			// Code that uses jQuery's $ can follow here.

			$.get("/wp-json/wp/v2/tags", function(data) {
				jQuery.data(document.body, "tags", data);
			});

			$('.filters .chip').click(function() {
				$('.filters .chip').not($(this)).removeClass('active');
				var selected = $(this).addClass('active').attr('data-category');
				$('.gallery .project').filter('[data-category*=' + selected + ']').parent().removeClass('hidden');
				$('.gallery .project').not('[data-category*=' + selected + ']').parent().addClass('hidden');
				setTimeout(function() {
					$('.gallery').masonry({
						columnWidth: 1,
						itemSelector: '>div',
					});
				}, 350);
			});



			$('.project .excerp .open').click(function() {
				var post = $(this).closest('.project').attr('data-post_id');

				let i;
				$.get("/wp-json/wp/v2/posts/" + post + "", function(data) {

					$(".project-overlay .cont h1").html(data.title.rendered);
					$(".project-overlay .parallax-cont img").attr('src', data.acf.kuva.sizes.large);
					$(".project-overlay .maincont .inner").html(data.content.rendered);
					$(".project-overlay .tags").html("");
					$(".project-overlay").addClass("loaded");

					for (i in data.tags) {
						var tag = jQuery.data(document.body, "tags").filter(tag => tag.id == data.tags[i])
						if (tag.length) {
							$(".project-overlay .tags").append("<li>" + tag[0].name + "</li>");
						}

					}

					$('.wp-block-image a').featherlightGallery({
						targetAttr: 'href',
					});


				});
				$('.project-overlay').addClass('active');
				$('body').addClass('no-scroll');


			});

			$('.project .extended .bg, .navbar, .navbar-collapse, .project-overlay .loader').click(function() {
				$('.project-overlay').removeClass('active').removeClass('loaded');
				$('body').removeClass('no-scroll');

			});
		});

		jQuery(window).load(function() {
			jQuery('.gallery').masonry();
			jQuery('.parallax-cont').parallaxjs()
		});


		document.addEventListener('lazybeforeunveil', function(e) {
			if ($(e.target).parents('.gallery').length) {
				jQuery('.gallery').masonry();
			}

		});


	},
};
