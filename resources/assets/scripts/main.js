// import external dependencies
import 'jquery';
import Masonry from 'masonry-layout';
import Parallaxjs from 'parallax-js';
import jQueryBridget from 'jquery-bridget';


import 'page-scroll-to-id';

// Import everything from autoload
import "./autoload/**/*"

// import local dependencies
import Router from './util/Router';
import common from './routes/common';
import home from './routes/home';
import aboutUs from './routes/about';

/** Populate Router instance with DOM routes */
const routes = new Router({
  // All pages
  common,
  // Home page
  home,
  // About Us page, note the change from about-us to aboutUs.
  aboutUs,
});

jQueryBridget( 'masonry', Masonry, $ );
jQueryBridget( 'parallaxjs', Parallaxjs, $ );
$(".scroll-to").mPageScroll2id();
$('body').scrollspy({ target: '#navbar-example' });

// Load Events
jQuery(document).ready(() => routes.loadEvents());
