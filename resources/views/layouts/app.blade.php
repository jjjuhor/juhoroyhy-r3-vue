<!doctype html>
<html {!! get_language_attributes() !!}>
  @include('partials.head')
  <body @php body_class() @endphp>
    <script>
      var app = new Vue({
        el: '#app',
        data: {
          message: 'Hello Vue!'
        }
      })
      </script>
    @php do_action('get_header') @endphp
    @include('partials.header')
  <div class="content">
  <div id="app">{{value}}</div>
	<main class="main">
	  @yield('content')
	</main>
  </div>
    @php do_action('get_footer') @endphp
    @include('partials.footer')
    @php wp_footer() @endphp
  </body>
</html>
