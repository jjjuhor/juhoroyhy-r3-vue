<section id="front" class="hero" data-relative-input="true">
	<div class="bg">
		<div class="parallax parallax-2" data-depth="0.6"></div>
		<div class="parallax parallax-1" data-depth="0.2"></div>
	</div>
	@php $hero = get_field('hero')
	@endphp
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-6 hero-title">
				<h1>
					@php echo $hero['hero_title']
					@endphp</h1>
					<p>
						@php echo $hero['hero_description']
						@endphp</p>
						<a href="#about-me" class="scroll-to btn btn-primary">Learn more</a>
						<a href="#contact" class="scroll-to btn btn-secondary">Contact me</a>
			</div>
		</div>
	</div>
</section>

<section id="about-me" class="about-me inverted">
	@php $aboutme = get_field('about_me')
	@endphp
	<div class="container-fluid">
		@php

		// check if the flexible content field has rows of data
		if( have_rows('skills') ):
		@endphp
		<div class="skill-cards row">

			@php
			// loop through the rows of data
			while ( have_rows('skills') ) : the_row();
			@endphp

			<div class="col-lg-4">
				<div class="skill-card">
					<div class="front">
						<div class="icon" style="background-image:url(@php the_sub_field('icon')@endphp)"></div>
						<div class="bottom">
							<h2>
								@php the_sub_field('title');
								@endphp</h2>
								<p>Hover to see more!</p>
						</div>
					</div>
					<div class="back">
						<h2>
							@php the_sub_field('title');
							@endphp</h2>
							<p>
								@php the_sub_field('description');
								@endphp</p>
								@php
								// check if the nested repeater field has rows of data
								if( have_rows('content') ):

								// loop through the rows of data
								while ( have_rows('content') ) : the_row();
								@endphp
								<div class="block">
									<h3>
										@php the_sub_field('subtitle');
										@endphp</h3>
										@php
										while ( have_rows('tags') ) : the_row();
										@endphp
										<div class="chip">
											@php the_sub_field('tag');
											@endphp</div>

											@php
											endwhile;
											@endphp
										</div>
										@php
										endwhile;



										endif;
										@endphp

								</div>
					</div>
				</div>

				@php
				endwhile;
				@endphp
			</div>
			@php
			else :

			// no layouts found

			endif;

			@endphp

			<div class="row">
				<div class="col-lg-6 about-me-text">
					<div class="two-lined">
						<span>Who am I?</span>
						<h1>About me </h1>
					</div>
					<p class="ingress">
						@php echo $aboutme['ingress']
						@endphp</p>
						@php echo $aboutme['main_text']
						@endphp
						<a href="#gallery" class="scroll-to btn btn-primary">My Works</a>
						<a href="#my-story" class="scroll-to btn btn-secondary">My Story</a>

				</div>
				<div class="col-lg-6 my-picture d-lg-block d-none">
					<img class="lazyload" data-src="@php echo $aboutme['picture'] @endphp">
				</div>
			</div>

			<div class="instagram ">

				<h3><span class="icon insta"></span> My latest Instagram posts</h2>
					@php
					the_field('instagram');
					@endphp

			</div>





		</div>
</section>


<section id="my-story" class="my-story inverted">
	<div class="container-fluid">
		<div class="two-lined">
			<span>Path to present</span>
			<h1>My Story</h1>
		</div>

		<div class="timeline">

			@php
			global $post;
			$get_posts = array(
			'post_type' => 'post',
			'orderby' => 'asc',
			'category_name' => 'timeline',
			'order' => 'ASC',
			'posts_per_page' => '25'
			);

			$posts = new WP_Query( $get_posts );
			if ( $posts->have_posts() ) : while ( $posts->have_posts() ) : $posts->the_post();
			@endphp

			<div class="event">
				<div class="year">
					<?php the_field('date');?>
				</div>
				<div class="desc">
					<h2>
						<?php the_title();?>
					</h2>
					<p>
						<?php the_field('content');?>
					</p>
				</div>
			</div>
			@endwhile
			@endif
		</div>

	</div>
</section>


<section id="gallery" class="my-works inverted">
	<div class="container-fluid">
		<div class="two-lined">
			<span>Showcase of my expertise</span>
			<h1>Gallery</h1>
		</div>
		<div class="filters">
			<div data-category="fav" class="chip active">Favorites</div>
			<div data-category="3d" class="chip">3D</div>
			<div data-category="art" class="chip">Art</div>
			<div data-category="animation" class="chip">Animation</div>
			<div data-category="branding" class="chip">Branding</div>
			<div data-category="design" class="chip">Graphic Design</div>
			<div data-category="web" class="chip">Web & Apps</div>

		</div>
	</div>

	<div class="gallery row">

		@php
		$count = 0;
		global $post;
		$get_posts = array(
		'post_type' => 'post',
		'orderby' => 'title',
		'category_name' => 'Keissit',
		'order' => 'ASC',
		'posts_per_page' => '100'
		);

		$posts = new WP_Query( $get_posts );
		if ( $posts->have_posts() ) : while ( $posts->have_posts() ) : $posts->the_post();
		@endphp

		<div class="col-xl-3 col-lg-4">
			<div class="project visible" data-category="@php the_field('tyyppi') @endphp" data-post_id="@php the_ID() @endphp" >

				<div class="bg"></div>

				<div class="excerp">
					<img class="lazyload thumb" data-src="<?php echo get_field_object('kuva')['value']['sizes']['medium']; ?>" src=""/>
						<div class="desc">
							<h2>
								<?php the_title();?>
							</h2>
							<div class="extra-content">
								<p>
									<?php the_field('sdesc');?>
								</p>
								<div class="btn btn-secondary">Read more</div>
							</div>
						</div>
					<div class="open"></div>
				</div>
			</div>
		</div>
		@php
		$count++;
		@endphp
		@endwhile
		@endif

	</div>


</section>

<section id="contact" class="contact inverted">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-6">
				<div class="two-lined">
					<span>Connect with me</span>
					<h1>Contact</h1>
				</div>


				@php the_post(); the_field('contact-form')
				@endphp
			</div>
			<div class="offset-md-3"></div>
			<div class="col-md-3 social-media">
				<a href="mailto:juho@juhoroyhy.fi">
					<span class="icon mail"></span>
					<span class="text">juho
						@juhoroyhy.fi</span>
				</a>
				<a href="https://www.instagram.com/juhoroyhy/">
					<span class="icon insta"></span>
					<span class="text">instagram/juhoroyhy</span>
				</a>
				<a href="https://www.linkedin.com/in/juhoroyhy/">
					<span class="icon linkedin"></span>
					<span class="text">linkedin/juhoroyhy</span>
				</a>
			</div>
		</div>
	</div>
</section>


<div class="project-overlay row">
	<div class="loader">
		<div class="lds-heart">
			<div></div>
		</div>

	</div>
	<div class="parallax-cont" data-relative-input="true">
		<div class="intro col-lg-6 ">
			<div class="cont">
				<div class="pre">Showcase</div>
				<h1>
				</h1>

				<div class="additional">
					<ul class="tags">

					</ul>
					<a href="#article-@php echo $count @endphp" class="btn btn-primary">Read more</a>

					@php /*
					// check if the repeater field has rows of data
					if( have_rows('links') ): ?>
					<div clasS="links-more">
						<ul class="project-links">
							<?php
						// loop through the rows of data
						while ( have_rows('links') ) : the_row();

						// display a sub field value
													?>
							<li>
								<a target="_blank" href="<?php echo  the_sub_field('url'); ?>" class="palvelu"><img class="icon" src="<?php echo the_sub_field('icon');?>" />
									<?php echo the_sub_field('palvelu'); ?></a>
							</li>
							<?php

						endwhile;
													?>

						</ul>
					</div>

					<?php
						else :

						// no rows found

					endif;*/
					@endphp

				</div></div>
		</div>
			  <img class="lazyload parallax" data-src="@php echo get_field_object('kuva')['value']['sizes']['large'] @endphp" data-depth="0.2" />
	</div>
	<div id="article-@php echo $count @endphp" class="maincont">
		<div class="inner">
		@php the_content() @endphp
		</div>
	</div>
	<div class="bg"></div>
</div>
</div>


		{!! wp_link_pages(['echo' => 0, 'before' => '<nav class="page-nav">
			<p>' . __('Pages:', 'sage'), 'after' => '</p>
		</nav>']) !!}
