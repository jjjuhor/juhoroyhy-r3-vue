<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no maximum-scale=1 minimum-scale=1">
    <meta property="og:title" content="Juho Röyhy's Portfolio - Design, technology, business" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="http://www.juhoroyhy.fi/" />
    <meta property="og:image" content="@asset('images/juhoroyhy-portfolio-banner.jpg')" />
    <link href="https://cdn.rawgit.com/h-ibaldo/Raleway_Fixed_Numerals/master/css/rawline.css" rel="stylesheet">
    @php wp_head() @endphp
</head>
