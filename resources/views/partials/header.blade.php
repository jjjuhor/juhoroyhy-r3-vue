<header class="banner">
	<nav class="navbar navbar-expand-md fixed-top">
		<div class="container-fluid">
		<a class="scroll-to navbar-brand" data-scroll href="#front"><img alt="juho royhy emblem" src="@php echo get_template_directory_uri() @endphp/assets/images/juho-royhy-emblem-2.png"/></a>
		<a class="navicon-button x collapsed d-md-none" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
		  <div class="navicon"></div>
		</a>
		<div class="collapse navbar-collapse" id="navbarNavDropdown">
			<div class="container-fluid" id="navbar-example">
				<div class="back-projects">Back to projects</div>
				<ul class="navbar-nav">
					<li class="nav-item active">
						<a class="scroll-to nav-link"  data-scroll href="#front">Home <span class="sr-only">(current)</span></a>
					</li>
					<li class="nav-item">
						<a class="scroll-to nav-link" data-scroll href="#about-me">About me</a>
					</li>
					<li class="nav-item">
						<a class="scroll-to nav-link"  data-scroll href="#my-story">My Story</a>
					</li>
					<li class="nav-item">
						<a class="scroll-to nav-link" data-scroll href="#gallery">Gallery</a>
					</li>
					<li class="nav-item">
						<a class="scroll-to nav-link"  data-scroll href="#contact">Contact</a>
					</li>
				</ul>
			</div>
		</div>
		</div>
	</nav>
</header>

<script>
jQuery('.navbar-nav').on('click', function(){
    jQuery('.navbar-collapse').collapse('hide');
	jQuery(".navicon-button").removeClass('open');
});

jQuery(".navicon-button").click(function(){
  jQuery(this).toggleClass("open");
});

  </script>
